const anyAmountInArray = array => array.reduce((acc, cur) => cur === "any" ? acc + 1 : acc, 0)
const getArrayWithRemovedItemByIndex = (array, index) => [...array.slice(0, index), ...array.slice(index + 1)]

exports.attempt = (available, allowed, preferred) => {
    const allowedAndAvailable = available.filter(number => allowed.includes(number));
    const anyAllowedAmount = anyAmountInArray(allowed);

    const allowedResult = [...allowedAndAvailable, ...Array(anyAllowedAmount).fill("any")]

    if (preferred.includes("any") && allowed.includes("any")) {
        return [...available]
    } else if (preferred.includes("any")) {
        return allowedResult.filter(Number)
    }

    const { result } = preferred.reduce((acc, cur) => {
        if (!acc.available.length || !acc.allowed.length) {
            return { ...acc }
        }

        const allowedAnyIndex = acc.allowed.indexOf("any");

        if (allowedAnyIndex === -1) {
            let indexBiggerOrSame = acc.allowed.findIndex(item => item >= cur);

            if (indexBiggerOrSame !== -1) {
                const availableIndex = acc.available.findIndex(item => item === acc.allowed[indexBiggerOrSame]);

                return {
                    result: [...acc.result, acc.allowed[indexBiggerOrSame]],
                    available: getArrayWithRemovedItemByIndex(acc.available, availableIndex),
                    allowed: getArrayWithRemovedItemByIndex(acc.allowed, indexBiggerOrSame)
                }
            } else {
                const availableIndex = acc.available.findIndex(item => item === acc.allowed[0]);

                return {
                    result: [...acc.result, acc.allowed[0]],
                    available: getArrayWithRemovedItemByIndex(acc.available, availableIndex),
                    allowed: getArrayWithRemovedItemByIndex(acc.allowed, 0)
                }
            }
        } else {
            let indexBiggerOrSame = acc.available.findIndex(item => item >= cur)

            if (indexBiggerOrSame !== -1) {                
                const indexOfAllowed = acc.allowed.findIndex(item => item === acc.available[indexBiggerOrSame])
                
                if (indexOfAllowed !== -1) {
                    return {
                        result: [...acc.result, acc.available[indexBiggerOrSame]],
                        available: getArrayWithRemovedItemByIndex(acc.available, indexBiggerOrSame),
                        allowed: getArrayWithRemovedItemByIndex(acc.allowed, indexOfAllowed),
                    }
                } else {
                    return {
                        result: [...acc.result, acc.available[indexBiggerOrSame]],
                        available: getArrayWithRemovedItemByIndex(acc.available, indexBiggerOrSame),
                        allowed: getArrayWithRemovedItemByIndex(acc.allowed, allowedAnyIndex),
                    }
                }
                
                
            } else {
                const indexOfAllowed = acc.allowed.findIndex(item => item === acc.available[0])
                
                if (indexOfAllowed !== -1) {
                    return {
                        result: [...acc.result, acc.available[indexBiggerOrSame]],
                        available: getArrayWithRemovedItemByIndex(acc.available, 0),
                        allowed: getArrayWithRemovedItemByIndex(acc.allowed, indexOfAllowed),
                    }
                } else {
                    return {
                        result: [...acc.result, acc.available[0]],
                        available: getArrayWithRemovedItemByIndex(acc.available, 0),
                        allowed: getArrayWithRemovedItemByIndex(acc.allowed, allowedAnyIndex)
                    }
                }
            }
        }
    }, {
        result: [],
        available,
        allowed: allowedResult
    })

    return result
}