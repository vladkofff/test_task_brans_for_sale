const getArrayWithRemovedItemByIndex = (array, index) => [...array.slice(0, index), ...array.slice(index + 1)]

exports.attempt = (available, allowed, preferred) => {
    const allowedAndAvailable = allowed.includes("any") ? available : available.filter(number => allowed.includes(number));

    if (preferred.includes("any")) {
        return allowedAndAvailable
    } else {
        const { result } = preferred.reduce((acc, cur) => {
            if (!acc.available.length) {
                return {
                    ...acc
                }
            }
            
            const biggerNumberIndex = acc.available.findIndex(number => number >= cur);

            if (biggerNumberIndex !== -1) {
                return {
                    available: getArrayWithRemovedItemByIndex(acc.available, biggerNumberIndex),
                    result: [...acc.result, acc.available[biggerNumberIndex]]        
                }
            } else {
                return {
                    available: getArrayWithRemovedItemByIndex(acc.available, 0),
                    result: [...acc.result, acc.available[0]]        
                }
            }
        }, {
            available: allowedAndAvailable,
            result: []
        })

        return result;
    }


}