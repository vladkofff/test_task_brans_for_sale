const expect = require("chai").expect;
const { attempt } = require("../anotherVision");

describe("tests anotherVision", function () {

    describe("without any", function () {
        it("1", function () {
            expect(JSON.stringify(attempt([240, 360, 720], [360, 720], [1080]))).to.equal(JSON.stringify([360]));
        });

        it("2", function () {
            expect(JSON.stringify(attempt([240, 720], [360, 720], [1080]))).to.equal(JSON.stringify([720]));
        });

        it("3", function () {
            expect(JSON.stringify(attempt([240], [360, 720], [1080]))).to.equal(JSON.stringify([]));
        });

        it("4", function () {
            expect(JSON.stringify(attempt([240, 360, 720], [240, 360, 720, 1080], [240, 360]))).to.equal(JSON.stringify([240, 360]));
        });

        it("5", function () {
            expect(JSON.stringify(attempt([240, 720], [240, 360, 720, 1080], [240, 360]))).to.equal(JSON.stringify([240, 720]));
        });

        it("6", function () {
            expect(JSON.stringify(attempt([240, 720], [240, 360, 1080], [240, 360]))).to.equal(JSON.stringify([240]));
        });

        it("7", function () {
            expect(JSON.stringify(attempt([720], [240, 360, 1080], [240, 360]))).to.equal(JSON.stringify([]));
        });

        it("8", function () {
            expect(JSON.stringify(attempt([240, 360], [240, 360], [720, 1080]))).to.equal(JSON.stringify([360]));
        });

    });

    describe("with any", function () {
        it("1", function () {
            expect(JSON.stringify(attempt([240, 360, 720], [360, "any"], [360, 720]))).to.equal(JSON.stringify([360, 720]));
        });

        it("2", function () {
            expect(JSON.stringify(attempt([240, 360, 720], [240, 360, 720], ["any", 720]))).to.equal(JSON.stringify([240, 360, 720]));
        });

        it("3", function () {
            expect(JSON.stringify(attempt([240, 360, 720], [360, 1080], ["any", 720]))).to.equal(JSON.stringify([360]));
        });

        it("4", function () {
            expect(JSON.stringify(attempt([240, 360, 720], [1080], ["any", 720]))).to.equal(JSON.stringify([]));
        });
    });

});